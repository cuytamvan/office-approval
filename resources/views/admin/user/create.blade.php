@extends('layouts.dashboard.app')

@section('content')
<div class="row">
  <div class="col-md-8">
    <div class="m-portlet m-portlet--tab">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <span class="m-portlet__head-icon m--hide"> <i class="la la-gear"></i> </span>
            <h3 class="m-portlet__head-text">
              Tambah {{ $title }}
            </h3>
          </div>
        </div>
      </div>
      <form action="{{ route($route.'store') }}" class="m-form m-form--label-align-right" method="post">
      @csrf
      <div class="m-portlet__body">
        <div class="row">
          <div class="col-sm-12 col-md-6 mb-2">
            <div class="form-group m-form__group">
              <label>
                Nama
              </label>
              <input type="text" name="name" id="name" class="form-control m-input" placeholder="Masukan nama" autocomplete="off" required value="{{ old('name') }}">
            </div>
          </div>
          <div class="col-sm-12 col-md-6 mb-2">
            <div class="form-group m-form__group">
              <label>
                Email
              </label>
              <input type="text" name="email" id="email" class="form-control m-input" placeholder="Masukan email" autocomplete="off" required value="{{ old('email') }}">
            </div>
          </div>
          <div class="col-sm-12 col-md-6 mb-sm-2">
            <div class="form-group m-form__group">
              <label>
                Password
              </label>
              <input type="password" name="password" id="password" class="form-control m-input" placeholder="Masukan password" autocomplete="off" required>
            </div>
          </div>
          <div class="col-sm-12 col-md-6 mb-sm-2">
            <div class="form-group m-form__group">
              <label>
                Konfirmasi Password
              </label>
              <input type="password" name="password_confirmation" id="password_confirmation" class="form-control m-input" placeholder="Ketik ulang password" autocomplete="off" required>
            </div>
          </div>
        </div>
      </div>
      <div class="m-portlet__foot m-portlet__foot--fit">
        <div class="m-form__actions">
          <button type="submit" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info">
            Simpan
          </button>
          <a href="{{ route($route.'index') }}" class="btn m-btn--pill m-btn--air btn-secondary">Batal</a>
        </div>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection