@extends('layouts.dashboard.app')
@section('style')
<style>
  .form-confirm{

  }
  .form-confirm .content{
    display: none;
  }
  .form-confirm .content.active{
    display: block;
  }
</style>
@endsection
@section('content')
<div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">
          Konfirmasi
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            &times;
          </span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-confirm">
          <div class="content active" id="content-1">
            <div class="row">
              <div class="col-sm-12 col-md-6 form-group m-form__group">
                <label>
                  No Mid
                </label>
                <input type="text" name="no_mid" id="no_mid" class="form-control m-input" autocomplete="off" disabled>
              </div>
              <div class="col-sm-12 col-md-6 form-group m-form__group">
                <label>
                  Nama Lengkap
                </label>
                <input type="text" name="name" id="name" class="form-control m-input" autocomplete="off" disabled>
              </div>
              <div class="col-sm-12 col-md-6 form-group m-form__group">
                <label>
                  NPWP
                </label>
                <input type="text" name="tin" id="tin" class="form-control m-input" autocomplete="off" disabled>
              </div>
              <div class="col-sm-12 col-md-6 form-group m-form__group">
                <label>
                  Tempat lahir
                </label>
                <input type="text" name="pob" id="pob" class="form-control m-input" autocomplete="off" disabled>
              </div>
              <div class="col-sm-12 col-md-6 form-group m-form__group">
                <label>
                  Tanggal bayar
                </label>
                <input type="text" name="pay_date" id="pay_date" class="form-control m-input datepicker" autocomplete="off" style="width: 100%;" disabled>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6 offset-sm-6 text-right">
                <button type="button" class="btn btn-info move-slide" data-target="#content-2">Selanjutnya</button>
              </div>
            </div>
          </div>
          <div class="content" id="content-2">
            <div class="row">
              <div class="col-sm-12 form-group m-form__group">
                <label>
                  Alamat Sesuai KTP
                </label>
                <input type="text" name="address" id="address" class="form-control m-input" autocomplete="off" disabled>
              </div>
              <div class="col-sm-12 form-group m-form__group">
                <label>
                  Alamat Domisili
                </label>
                <input type="text" name="residence_address" id="residence_address" class="form-control m-input" autocomplete="off" disabled>
              </div>
              <div class="col-sm-12 form-group m-form__group">
                <label>
                  Kode Pos
                </label>
                <input type="text" name="postal_code" id="postal_code" class="form-control m-input" autocomplete="off" disabled>
              </div>
              <div class="col-sm-12 col-md-6 form-group m-form__group">
                <label>
                  Email
                </label>
                <input type="email" name="email" id="email" class="form-control m-input" autocomplete="off" required disabled>
              </div>
              <div class="col-sm-12 col-md-6 form-group m-form__group">
                <label>
                  No Hp
                </label>
                <input type="text" name="phone_number" id="phone_number" class="form-control m-input" autocomplete="off" style="width: 100%;" disabled>
              </div>
              <div class="col-sm-12 form-group m-form__group">
                <label>
                  No Whatsapp
                </label>
                <input type="text" name="whatsapp_number" id="whatsapp_number" class="form-control m-input" autocomplete="off" disabled>
              </div>
            </div>
            <form id="form-confirmation">
              <input type="hidden" name="id" id="id">
              <div class="row">
                <div class="col-sm-6">
                  <button type="button" class="btn btn-secondary move-slide" data-target="#content-1">Sebelumnya</button>
                </div>
                <div class="col-sm-6 text-sm-right">
                  <button type="submit" id="buttonApprove" class="btn btn-primary">Simpan</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="m-portlet m-portlet--tab">
  <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
      <div class="m-portlet__head-title">
        <span class="m-portlet__head-icon m--hide"> <i class="la la-gear"></i> </span>
        <h3 class="m-portlet__head-text">
          Daftar {{ $title }}
        </h3>
      </div>
    </div>
  </div>
  <div class="m-portlet__body">
    @include('layouts.dashboard.inc.datatable', [
      'hiddenCreate' => true
    ])
  </div>
</div>
@endsection
@section('script')
<script src="{{ asset('dashboard/vendors/custom/jquery.form.js') }}"></script>
<script>
  var url_data      = '{{ route($route.'data_api_confirmation') }}';
  var data = [];
  var custom_column = [{
    field     : "User.name",
    title     : "Nama",
    sortable  : 'asc',
    filterable: false,
    width     : 150,
    template  : function(r){
      data = [...data, r];
      return r.name;
    }
  }, {
    field     : "User.email",
    title     : "Email",
    sortable  : 'asc',
    filterable: false,
    width     : 150,
    template  : function(r){
      return r.email;
    }
  }, {
    field     : "User.created_at",
    title     : "Dibuat",
    sortable  : 'asc',
    filterable: false,
    width     : 150,
    template  : function(r){
      var created_at = moment(r.created_at, 'YYYY-MM-DD HH:mm:ss').format('DD MMMM YYYY HH:mm:ss');
      return created_at;
    }
  }, {
    field     : "action",
    title     : "Aksi",
    sortable  : 'asc',
    filterable: false,
    width     : 150,
    template  : function(r){
      var url = "{{ route($route.'index') }}";
      var btn = `<a href="${url}/${r.id}" class="btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa fa-eye"></i></a>&nbsp;`;
      btn += `<button data-toggle="modal" data-id="${r.id}" data-target="#modal-confirm" class="btn btn-info m-btn m-btn--icon m-btn--icon-only m-btn--pill open-confirm-modal"><i class="fa fa-check"></i></button>&nbsp;`;
      return btn;
    }
  }];
  $(document).ready(function () {
    $('.datepicker').datepicker({
      format: 'yyyy-mm-dd'
    });

    $('.move-slide').click(function(){
      var target = $(this).data('target');

      $('.form-confirm .content').removeClass('active');
      $(target).addClass('active');
    });

    $('body').on('click', '.open-confirm-modal', function(){
      $('.form-confirm .content').removeClass('active');
      $('.form-confirm #content-1').addClass('active');

      var id = $(this).data('id');

      resetFormConfirmation();
      var findData = data.find(r => r.id == id);
      Object.keys(findData).forEach(k => {
        $(`#${k}`).val(findData[k]);
      });
      var bA = $('#buttonApprove');
      if(findData.status == '1'){
        bA.removeClass('btn-primary');
        bA.addClass('btn-danger');
        bA.html('Tolak');
      }else{
        bA.removeClass('btn-danger');
        bA.addClass('btn-primary');
        bA.html('Setuju');
      }
    });

    $('#form-confirmation').submit(function(e){
      e.preventDefault();
      var data = {
        id: $('#id').val(),
        _token: '{{ csrf_token() }}'
      };
      console.log(data);
      $.ajax({
        type     : 'post',
        url      : '{{ route($route.'confirmation.update') }}',
        data     : data,
        dataType : 'json',
        success  : function (res) {
          if(res.success){
            swal('Berhasil', 'Berhasil diapprove', 'success');
            $('#modal-confirm').modal('hide');
          }else{
            swal('Gagal', 'Gagal diapprove', 'error');
          }
        }
      });
    })
  });

  function resetFormConfirmation(){
    $('.form-confirm .content input').val('');
  }
</script>
@endsection