@extends('layouts.dashboard.app')

@section('content')
<div class="m-portlet m-portlet--tab">
  <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
      <div class="m-portlet__head-title">
        <span class="m-portlet__head-icon m--hide"> <i class="la la-gear"></i> </span>
        <h3 class="m-portlet__head-text">
          Daftar {{ $title }}
        </h3>
      </div>
    </div>
  </div>
  <div class="m-portlet__body">
    @include('layouts.dashboard.inc.datatable', [
      'hiddenCreate' => true
    ])
  </div>
</div>
@endsection
@section('script')
<script>
  var url_data      = '{{ route($route.'data_api') }}';
  var custom_column = [{
      field     : "Agent.id",
      title     : "#",
      sortable  : 'asc',
      filterable: false,
      width     : 20,
      template  : function(r){
        return r.id;
      }
    }, {
      field     : "Agent.name",
      title     : "Nama",
      sortable  : 'asc',
      filterable: false,
      width     : 150,
      template  : function(r){
        return r.name;
      }
    }, {
      field     : "Agent.email",
      title     : "Email",
      sortable  : 'asc',
      filterable: false,
      width     : 150,
      template  : function(r){
        return r.email;
      }
    }, {
      field     : "Agent.created_at",
      title     : "Dibuat",
      sortable  : 'asc',
      filterable: false,
      width     : 150,
      template  : function(r){
        var created_at = moment(r.created_at, 'YYYY-MM-DD HH:mm:ss').format('DD MMMM YYYY HH:mm:ss');
        return created_at;
      }
    }, {
      field     : "action",
      title     : "Aksi",
      sortable  : 'asc',
      filterable: false,
      width     : 120,
      template  : function(r){
        var url = "{{ route($route.'index') }}";
        var btn = `<a href="${url}/${r.id}" class="btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa fa-eye"></i></a>&nbsp;`;
        return btn;
      }
    }];
</script>
@endsection