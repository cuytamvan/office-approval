@extends('layouts.dashboard.app')

@section('content')
<div class="row">
  <div class="col-md-3">
    <div class="m-portlet m-portlet--tab">
      <div class="m-portlet__body p-0">
        <div class="m-card-profile">
          <div class="m-card-profile__pic pt-0 pb-0">
            <div class="m-card-profile__pic-wrapper">
              <img src="{{ asset('dashboard/user.svg') }}" style="width: 100%;">
            </div>
          </div>
        </div>
      </div>
      <div class="m-portlet__foot m-portlet__foot--fit text-center pt-4 pb-4">
        <a href="{{ route('admin.dashboard') }}" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info">Kembali</a>
      </div>
    </div>
  </div>
  <div class="col-md-9">
    <div class="m-portlet m-portlet--tab">
      <form action="{{ route('admin.profile_action') }}" method="post">
        @csrf
        <div class="m-form">
          <div class="m-portlet__body">
            <div class="form-group m-form__group row">
              <div class="col-10 ml-auto">
                <h3 class="m-form__section">
                  Data Pribadi
                </h3>
              </div>
            </div>
            @php
              
              $data = [
                (object) [
                  'name' => 'name',
                  'text' => 'Nama',
                  'value' => $user->name
                ],
                (object) [
                  'name' => 'username',
                  'text' => 'Username',
                  'value' => $user->username,
                  'disabled' => true
                ],
                (object) [
                  'name' => 'password',
                  'text' => 'Password',
                  'type' => 'password',
                ],
                (object) [
                  'name' => 'password_confirmation',
                  'text' => 'Konfirmasi Password',
                  'type' => 'password',
                ],
              ]
            @endphp
            @foreach ($data as $r)
              <div class="form-group m-form__group row m-0 p-1">
                <label for="example-text-input" class="col-4 col-form-label">
                  {{ $r->text }}
                </label>
                <div class="col-7">
                  <input type="{{ isset($r->type) ? $r->type : 'text' }}" name="{{ $r->name }}" id="{{ $r->name }}" class="form-control m-input" value="{{ isset($r->value) ?old($r->name, $r->value) : '' }}" autocomplete="off" {{ isset($r->disabled) && $r->disabled ? 'disabled' : '' }} >
                </div>
              </div>
            @endforeach
          </div>
          <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
              <button type="submit" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info">
                Simpan perubahan
              </button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection