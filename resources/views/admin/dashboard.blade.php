@extends('layouts.dashboard.app')

@section('style')
  <style>
    .icon-dashboard{
      font-size: 25px;
    }
  </style>
@endsection

@section('content')
<div class="row">
  <div class="col-sm-12 col-md-6">
    <div class="m-portlet m-portlet--tab">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
              {{ $title }}
            </h3>
          </div>
        </div>
      </div>
      <div class="m-portlet__body">
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum delectus perspiciatis aliquid cupiditate sequi maiores, nisi fugiat dolore exercitationem reprehenderit.</p>
      </div>
    </div>
  </div>
</div>
@endsection