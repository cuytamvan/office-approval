@extends('layouts.dashboard.app')

@section('content')
{{-- <div class="container"> --}}
  <div class="row">
    <div class="col-md-8 col-lg-7 col-xl-5">
      <div class="m-portlet m-portlet--tab">
        <div class="m-form">
          <div class="m-portlet__body">
            <div class="form-group m-form__group row">
              <div class="col-10 ml-auto">
                <h3 class="m-form__section">
                  Detail
                </h3>
              </div>
            </div>
            @php
              $detail = [
                (Object) [
                  'label' => 'Nama',
                  'value' => $data->name
                ],
                (Object) [
                  'label' => 'Email',
                  'value' => $data->email
                ],
                (Object) [
                  'label' => 'Dibuat',
                  'value' => Dashboard::indonesiaDateFormat($data->created_at, true)
                ],
              ]
            @endphp

            @foreach ($detail as $r)
            <div class="form-group m-form__group row mt-0">
              <label for="example-text-input" class="col-3 col-form-label">
                {{ $r->label }}
              </label>
              <div class="col-7">
                @if (isset($r->textarea) && $r->textarea == true)
                  <textarea disabled class="form-control m-input" rows="4">{{ $r->value }}</textarea>
                @else
                <input class="form-control m-input" type="text" value="{{ $r->value }}" disabled>
                @endif
              </div>
            </div>
            @endforeach
          </div>
          <div class="m-portlet__foot m-portlet__foot--fit p-4">
            <a href="{{ route($route.'index') }}" class="btn m-btn--pill m-btn--air m-btn m-btn--gradient-from-primary m-btn--gradient-to-info">Kembali</a>
          </div>
        </div>
      </div>
    </div>
  </div>
{{-- </div> --}}
@endsection