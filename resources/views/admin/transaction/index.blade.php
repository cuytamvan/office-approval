@extends('layouts.dashboard.app')
@section('style')
<style>
  .form-confirm{

  }
  .form-confirm .content{
    display: none;
  }
  .form-confirm .content.active{
    display: block;
  }
</style>
@endsection
@section('content')
<div class="modal fade" id="modal-detail" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">
          Detail
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            &times;
          </span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-confirm">
          <div class="content active" id="cis">
            <div class="row">
              <div class="col-sm-12 col-md-6 form-group m-form__group">
                <label>
                  Nomor Identitas
                </label>
                <input type="text" class="form-control m-input" autocomplete="off" disabled value="lorem ipsum dolor sit amet">
              </div>
              <div class="col-sm-12 col-md-6 form-group m-form__group">
                <label>
                  Nama Tertanggung
                </label>
                <input type="text" class="form-control m-input" autocomplete="off" disabled value="lorem ipsum dolor sit amet">
              </div>
              <div class="col-sm-12 form-group m-form__group">
                <label>
                  Alamat Tertanggung
                </label>
                <input type="text" class="form-control m-input" autocomplete="off" disabled value="lorem ipsum dolor sit amet">
              </div>
              <div class="col-sm-12 col-md-6 form-group m-form__group">
                <label>
                  Lokasi Pertanggungan
                </label>
                <input type="text" class="form-control m-input" autocomplete="off" disabled value="lorem ipsum dolor sit amet">
              </div>
              <div class="col-sm-12 col-md-6 form-group m-form__group">
                <label>
                  Nilai Pertanggungan
                </label>
                <input type="text" class="form-control m-input" autocomplete="off" disabled value="9">
              </div>
              <div class="col-sm-12 col-md-6 form-group m-form__group">
                <label>
                  Ahli waris
                </label>
                <input type="text" class="form-control m-input" autocomplete="off" disabled value="lorem ipsum dolor sit amet">
              </div>
              <div class="col-sm-12 col-md-6 form-group m-form__group">
                <label>
                  Nilai Pertanggungan
                </label>
                <input type="text" class="form-control m-input" autocomplete="off" disabled value="lorem ipsum dolor sit amet">
              </div>
            </div>
          </div>
          <div class="content" id="cit">
            <div class="row">
              <div class="col-sm-12 col-md-6 form-group m-form__group">
                <label>
                  Nomor Identitas
                </label>
                <input type="text" class="form-control m-input" autocomplete="off" disabled value="lorem ipsum dolor sit amet">
              </div>
              <div class="col-sm-12 col-md-6 form-group m-form__group">
                <label>
                  Nama Tertanggung
                </label>
                <input type="text" class="form-control m-input" autocomplete="off" disabled value="lorem ipsum dolor sit amet">
              </div>
              <div class="col-sm-12 form-group m-form__group">
                <label>
                  Alamat Tertanggung
                </label>
                <input type="text" class="form-control m-input" autocomplete="off" disabled value="lorem ipsum dolor sit amet">
              </div>
              <div class="col-sm-12 col-md-6 form-group m-form__group">
                <label>
                  Lokasi awal pertanggungan
                </label>
                <input type="text" class="form-control m-input" autocomplete="off" disabled value="lorem ipsum dolor sit amet">
              </div>
              <div class="col-sm-12 col-md-6 form-group m-form__group">
                <label>
                  Lokasi akhir pertanggungan
                </label>
                <input type="text" class="form-control m-input" autocomplete="off" disabled value="lorem ipsum dolor sit amet">
              </div>
              <div class="col-sm-12 col-md-6 form-group m-form__group">
                <label>
                  Nilai Pertanggungan
                </label>
                <input type="text" class="form-control m-input" autocomplete="off" disabled value="9">
              </div>
              <div class="col-sm-12 col-md-6 form-group m-form__group">
                <label>
                  Periode Pertanggungan
                </label>
                <input type="text" class="form-control m-input" autocomplete="off" disabled value="29/12/2012">
              </div>
              <div class="col-sm-12 form-group m-form__group">
                <label>
                  Jenis Angkutan yang di gunakan
                </label>
                <input type="text" class="form-control m-input" autocomplete="off" disabled value="9">
              </div>
              <div class="col-sm-12 col-md-6 form-group m-form__group">
                <label>
                  No Polisi Kendaraan
                </label>
                <input type="text" class="form-control m-input" autocomplete="off" disabled value="F 5672 KJ">
              </div>
              <div class="col-sm-12 col-md-6 form-group m-form__group">
                <label>
                  Ahli waris
                </label>
                <input type="text" class="form-control m-input" autocomplete="off" disabled value="lorem ipsum dolor sit amet">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="m-portlet m-portlet--tab">
  <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
      <div class="m-portlet__head-title">
        <span class="m-portlet__head-icon m--hide"> <i class="la la-gear"></i> </span>
        <h3 class="m-portlet__head-text">
          Daftar {{ $title }}
        </h3>
      </div>
    </div>
  </div>
  <div class="m-portlet__body">
    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
      <div class="row align-items-center">
        <div class="col-xl-8 order-2 order-xl-1">
          <div class="form-group m-form__group row align-items-center">
            <div class="col-md-4">
              <div class="m-input-icon m-input-icon--left">
                <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                <span class="m-input-icon__icon m-input-icon__icon--left">
                  <span>
                    <i class="la la-search"></i>
                  </span>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <table class="m-datatable" id="html_table" width="100%">
      <thead>
        <tr>
          <th title="No">No</th>
          <th title="Nama agent">Nama agent</th>
          <th title="Nama Produk">Nama Produk</th>
          <th title="Aksi">Aksi</th>
        </tr>
      </thead>
      <tbody>
        @php
          $data = [
            (object) [
              'nama_agent' => 'test',
              'product' => 'cis'
            ],
            (object) [
              'nama_agent' => 'test',
              'product' => 'cit'
            ],
          ];
          $no = 1;
        @endphp
        @foreach ($data as $r)
          <tr>
            <td>{{ $no }}</td>
            <td>{{ $r->nama_agent }}</td>
            <td>{{ $r->product }}</td>
            <td>
              <button data-toggle="modal" data-target="#modal-detail" data-product="#{{ $r->product }}" class="btn btn-success m-btn m-btn--icon m-btn--icon-only m-btn--pill btn-detail"><i class="fa fa-eye"></i></button>
            </td>
          </tr>
          @php
            $no++;
          @endphp
        @endforeach
      </tbody>
    </table>
  </div>
</div>
@endsection
@section('script')
<script>
var DatatableHtmlTableDemo = function() {
	var demo = function() {
		var datatable = $('.m-datatable').mDatatable({
			data: {
				saveState: {cookie: false},
			},
			search: {
				input: $('#generalSearch'),
			},
			columns: [],
		});
	};

	return {
		init: function() {
			demo();
		},
	};
}();

$(document).ready(function() {
	DatatableHtmlTableDemo.init();
  $('.btn-detail').click(function(){
    var prod = $(this).data('product');
    $('.form-confirm .content').removeClass('active');
    $(prod).addClass('active');
  })
});
</script>
@endsection