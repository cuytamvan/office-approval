@extends('layouts.dashboard.login', [
  'title' => 'Login admin',
  'header_right' => 'Join Our Community',
  'content_right' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo, architecto!',
])

@section('content')
<div class="m-login__head">
  <h3 class="m-login__title">
    Masuk sebagai admin
  </h3>
</div>
<form class="m-login__form m-form" method="POST" action="{{ route('admin.login_action') }}">
  @csrf
  <div class="form-group m-form__group">
    <input class="form-control m-input" type="text" placeholder="Email" name="email" autocomplete="off" value="{{ old('email') }}">
  </div>
  <div class="form-group m-form__group">
    <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password">
  </div>
  <div class="row m-login__form-sub">
    <div class="col m--align-left">
      <label class="m-checkbox m-checkbox--focus">
        <input type="checkbox" name="remember">
        Remember me
        <span></span>
      </label>
    </div>
  </div>
  <div class="m-login__form-action">
    <button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
      Sign In
    </button>
  </div>
</form>
@endsection