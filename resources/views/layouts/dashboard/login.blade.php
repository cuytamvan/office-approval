<!DOCTYPE html>
<html lang="en" >
	<head>
		<meta charset="utf-8" />
		<title>
			{{ config('app.name') }} {{ isset($title) ? '| '.$title : '' }}
		</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
      WebFont.load({
        google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
        active: function() {
            sessionStorage.fonts = true;
        }
      });
		</script>
		<link href="{{ asset('dashboard/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('dashboard/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="{{ asset('favicon.png') }}" />
	</head>
	<body  class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin" id="m_login">
				<div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
					<div class="m-stack m-stack--hor m-stack--desktop">
						<div class="m-stack__item m-stack__item--fluid">
							<div class="m-login__wrapper">
								<div class="m-login__logo">
									<a href="#">
										<img src="{{ asset('favicon.png') }}" style="width: 150px;">
									</a>
								</div>
								<div class="m-login__signin">
									@yield('content')
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1	m-login__content" style="background-image: url({{ asset('dashboard/app/media/img//bg/bg-4.jpg') }})">
					<div class="m-grid__item m-grid__item--middle">
						<h3 class="m-login__welcome">
							{{ isset($header_right) ? $header_right : '' }}
						</h3>
						<p class="m-login__msg">
							{{ isset($content_right) ? $content_right : '' }}
						</p>
					</div>
				</div>
			</div>
		</div>
		<script src="{{ asset('dashboard/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ asset('dashboard/demo/default/base/scripts.bundle.js') }}" type="text/javascript"></script>
		@include('sweetalert::view')
	</body>
</html>
