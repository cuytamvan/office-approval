<!DOCTYPE html>
<html lang="en" >
	<head>
		<meta charset="utf-8" />
		<title>
			{{ config('app.name') }} {{ isset($title) ? '| '.$title : '' }}
		</title>
		<meta name="description" content="Base laravel v1.1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
      WebFont.load({
        google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
        active: function() {
          sessionStorage.fonts = true;
        }
      });
		</script>

		<link href="{{ asset('dashboard/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('dashboard/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('dashboard/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="{{ asset('favicon.png') }}" />
    @yield('style')
    @stack('style')
	</head>

	<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
		<div class="m-grid m-grid--hor m-grid--root m-page">
			@include('layouts.dashboard.inc.header')
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
				<button class="m-aside-left-close  m-aside-left-close--skin-dark" id="m_aside_left_close_btn">
					<i class="la la-close"></i>
				</button>
        
        @include('layouts.dashboard.inc.sidebar')
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					@include('layouts.dashboard.inc.breadcrumb')

          <div class="m-content">
						@yield('content')
					</div>
				</div>
      </div>

			@include('layouts.dashboard.inc.footer')
		</div>

		<div id="m_scroll_top" class="m-scroll-top">
			<i class="la la-arrow-up"></i>
		</div>

		<script src="{{ asset('dashboard/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
		<script src="{{ asset('dashboard/demo/default/base/scripts.bundle.js') }}" type="text/javascript"></script>
		@yield('script')
		@stack('script')
		
    @if(!empty($validator))
			{!! $validator->render() !!}
    @endif

		@include('sweetalert::view')
		@include('layouts.dashboard.inc.alert')
	</body>
</html>
