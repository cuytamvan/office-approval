<div class="m-subheader">
  <div class="d-flex align-items-center">
    <div class="mr-auto">
      <h3 class="m-subheader__title {{ isset($breadcrumb) ? 'm-subheader__title--separator' : '' }}">
        {{ isset($title) ? $title : '' }}
      </h3>
      @if (isset($breadcrumb))
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          @php($no = 1)
          @foreach ($breadcrumb as $r)
            @if ($no > 1)
              <li class="m-nav__separator">/</li>
            @endif
            <li class="m-nav__item">
              <a href="{{ $r[1] }}" class="m-nav__link">
                <span class="m-nav__link-text">
                  {{ $r[0] }}
                </span>
              </a>
            </li>
            @php($no++)
          @endforeach
        </ul>
      @endif
    </div>
  </div>
</div>