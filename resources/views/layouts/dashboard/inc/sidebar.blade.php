@php
  $menu = [];
  if(Route::is('admin*')){
    $menu = [
      (object) [
        'text'     => 'Dashboard',
        'route'    => 'admin.dashboard',
        'icon'     => 'flaticon-line-graph',
        'children' => [
          
        ],
      ],

      (object) [
        'text' => 'Transaksi',
      ],

      (object) [
        'text'     => 'Transaksi',
        'route'    => 'admin.transaction.index',
        'icon'     => 'flaticon-diagram',
        'children' => [],
      ],

      (object) [
        'text' => 'Office',
      ],

      (object) [
        'text'     => 'Agent',
        'route'    => 'admin.agent',
        'icon'     => 'flaticon-users',
        'children' => [
          (object) [
            'text'  => 'Menunggu Konfirmasi',
            'route' => 'admin.agent.confirmation'
          ],
          (object) [
            'text'  => 'Daftar Agent',
            'route' => 'admin.agent.index'
          ],
        ],
      ],

      (object) [
        'text' => 'User',
      ],

      (object) [
        'text'     => 'Admin',
        'route'    => 'admin.user',
        'icon'     => 'flaticon-user-settings',
        'children' => [
          (object) [
            'text'  => 'Tambah Admin',
            'route' => 'admin.user.create'
          ],
          (object) [
            'text'  => 'Daftar Admin',
            'route' => 'admin.user.index'
          ],
        ],
      ],
    ];
  }
@endphp
<div id="m_aside_left" class="m-grid__item m-aside-left m-aside-left--skin-dark ">
  <div id="m_ver_menu" class="m-aside-menu m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark" m-menu-vertical="1" m-menu-scrollable="0" m-menu-dropdown-timeout="500">
    <ul class="m-menu__nav m-menu__nav--dropdown-submenu-arrow">
      @foreach ($menu as $r)
        @if (!isset($r->link) && !isset($r->route) && !isset($r->icon))
        <li class="m-menu__section">
          <h4 class="m-menu__section-text">
            {{ $r->text }}
          </h4>
          <i class="m-menu__section-icon flaticon-more-v3"></i>
        </li>
        @elseif (isset($r->children) && count($r->children) > 0)
        <li class="m-menu__item m-menu__item--submenu {{ Route::is($r->route.'*') ? 'm-menu__item--open m-menu__item--expanded' : '' }}" aria-haspopup="true" m-menu-submenu-toggle="hover">
          <a  href="javascript:;" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon {{   $r->icon }}"></i>
            <span class="m-menu__link-text">
              {{ $r->text }}
            </span>
            <i class="m-menu__ver-arrow la la-angle-right"></i>
          </a>
          <div class="m-menu__submenu ">
            <span class="m-menu__arrow"></span>
            <ul class="m-menu__subnav">
              @foreach ($r->children as $r1)
              <li class="m-menu__item {{ Route::is($r1->route) ? 'm-menu__item--active' : '' }}" aria-haspopup="true">
                <a href="{{ route($r1->route) }}" class="m-menu__link ">
                  <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                    <span></span>
                  </i>
                  <span class="m-menu__link-text">
                    {{ $r1->text }}
                  </span>
                </a>
              </li>
              @endforeach
            </ul>
          </div>
        </li>
        @else
        <li class="m-menu__item {{ Route::is($r->route) ? 'm-menu__item--active' : '' }}" aria-haspopup="true" >
          <a  href="{{ route($r->route) }}" class="m-menu__link">
            <i class="m-menu__link-icon {{ $r->icon }}"></i>
            <span class="m-menu__link-title">
              <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">
                  {{ $r->text }}
                </span>
                @if (isset($r->badge))
                <span class="m-menu__link-badge">
                  <span class="m-badge m-badge--danger">
                    {{ $r->badge }}
                  </span>
                </span>
                @endif
              </span>
            </span>
          </a>
        </li>
        @endif
      @endforeach
    </ul>
  </div>
</div>