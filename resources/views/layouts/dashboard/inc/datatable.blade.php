@push('style')
<style>
  .my_datatable *,
  .tiny-text{
    font-size: 12px!important;
  }
</style>
@endpush
<div class="row">
  <div class="col-md-3">
    <div class="m-input-icon m-input-icon--left">
      <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
      <span class="m-input-icon__icon m-input-icon__icon--left">
        <span>
          <i class="la la-search"></i>
        </span>
      </span>
    </div>
  </div>
  @if (isset($hiddenCreate) && $hiddenCreate == true)
  @else
  <div class="col-md-9 order-1 order-xl-2 m--align-right">
    <a href="{{ route($route.'create') }}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
      <span>
        <i class="flaticon-add"></i>
        <span>
          Tambah {{ $title }}
        </span>
      </span>
    </a>
  </div>
  @endif
</div>
<div class="my_datatable mt-4" id="datatable_"></div>

@push('script')
<script src="{{ asset('dashboard/app/js/custom-datatable.js') }}"></script>
@endpush