@if (count($errors->all()))
<script>
var errors = {!! json_encode($errors->all()) !!};
var html = '<p>';
errors.forEach(function(v, i){
  html += v + '<br>';
})
html += '</p>';
swal('Validasi gagal!', html, 'error')
</script>
@endif