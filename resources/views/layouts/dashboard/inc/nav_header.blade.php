<ul class="m-menu__nav  m-menu__nav--submenu-arrow">
  @php
    $menu = [
      (object) [
        'icon' => 'flaticon-add',
        'text' => 'Actions',
        'link' => '',
        'children' => [
          (object) [
            'icon' => 'flaticon-business',
            'text' => 'Generate Report',
            'link' => '',
            'children' => [
              (object) [
                'text' => 'Customer Feedback',
                'link' => ''
              ],
            ],
          ],
        ],
      ],
    ];
  @endphp

  @foreach ($menu as $r)
    @if (isset($r->children) && count($r->children)  > 0)
      <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel"  m-menu-submenu-toggle="click" m-menu-link-redirect="1" aria-haspopup="true">
        <a  href="javascript:;" class="m-menu__link m-menu__toggle">
          <i class="m-menu__link-icon {{ $r->icon }}"></i>
          <span class="m-menu__link-text">
            {{ $r->text }}
          </span>
          <i class="m-menu__hor-arrow la la-angle-down"></i>
          <i class="m-menu__ver-arrow la la-angle-right"></i>
        </a>
        <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left">
          <span class="m-menu__arrow m-menu__arrow--adjust"></span>
          <ul class="m-menu__subnav">
            @foreach ($r->children as $r1)
              @if (isset($r1->children) && count($r1->children)  > 0)
                <li class="m-menu__item  m-menu__item--submenu"  m-menu-submenu-toggle="hover" m-menu-link-redirect="1" aria-haspopup="true">
                  <a  href="javascript:;" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon {{ $r1->icon }}"></i>
                    <span class="m-menu__link-text">
                      {{ $r1->text }}
                    </span>
                    <i class="m-menu__hor-arrow la la-angle-right"></i>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                  </a>
                  <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--right">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                      @foreach ($r1->children as $r2)
                        <li class="m-menu__item" m-menu-link-redirect="1">
                          <a href="{{ $r2->link }}" class="m-menu__link ">
                            <span class="m-menu__link-text">
                              {{ $r2->text }}
                            </span>
                          </a>
                        </li>
                      @endforeach
                    </ul>
                  </div>
                </li>
              @else
                <li class="m-menu__item">
                  <a  href="{{ $r1->link }}" class="m-menu__link ">
                    <i class="m-menu__link-icon flaticon-file"></i>
                    <span class="m-menu__link-text">
                      {{ $r1->text }}
                    </span>
                    @if (isset($r->badge))
                      <span class="m-menu__link-badge">
                        <span class="m-badge m-badge--success">
                          {{ $r->badge }}
                        </span>
                      </span>
                    @endif
                  </a>
                </li>
              @endif
            @endforeach
          </ul>
        </div>
      </li>
    @else
    <li class="m-menu__item" m-menu-link-redirect="1">
      <a  href="{{ $r->link }}" class="m-menu__link">
        <i class="m-menu__link-icon {{ $r->icon }}"></i>
        <span class="m-menu__link-text">
          {{ $r->text }}
        </span>
      </a>
    </li>
    @endif
  @endforeach
</ul>