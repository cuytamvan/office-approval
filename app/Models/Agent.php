<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agent extends Authenticatable{
    use SoftDeletes;
    protected $fillable = [
        'name', 'email', 'status', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
