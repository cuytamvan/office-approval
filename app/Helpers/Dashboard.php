<?php

namespace App\Helpers;

class Dashboard{
  protected static function month(){
    return [
      '01' => 'Januari',
      '02' => 'Febuari',
      '03' => 'Maret',
      '04' => 'April',
      '05' => 'Mei',
      '06' => 'Juni',
      '07' => 'Juli',
      '08' => 'Agustus',
      '09' => 'September',
      '10' => 'Oktober',
      '11' => 'November',
      '12' => 'Desember',
    ];
  }

  public static function indonesiaDateFormat($date, $time = false){
    $date1   = date('Y-m-d', strtotime($date));
    $ex_date = explode('-', $date1);
    $month   = self::month();

    if($time){
      $time   = date('H:i:s', strtotime($date));
      return $ex_date[2].' '.$month[$ex_date[1]].' '.$ex_date[0].' '.$time;
    }
    return $ex_date[2].' '.$month[$ex_date[1]].' '.$ex_date[0];
  }
}