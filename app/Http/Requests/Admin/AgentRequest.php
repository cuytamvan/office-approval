<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

use Auth;
use Route;

class AgentRequest extends FormRequest
{
    public function authorize(){
        return $this->guard()->check();
    }

    protected function guard(){
        return Auth::guard();
    }

    public function rules(){
        $role = [
            'name'     => 'required|string|max:50',
            'email'    => 'required|email|max:50',
            'password' => 'required|confirmed',
        ];
        if(Route::is('admin.agent.update')){
            $role['password'] = 'confirmed';
        }
        return $role;
    }
}
