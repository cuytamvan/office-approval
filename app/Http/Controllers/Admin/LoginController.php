<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Admin;

use Auth;
use View;

class LoginController extends Controller{
    protected $view  = 'admin.';
    protected $route = 'admin.';

    public function showLoginForm(){
        return view($this->view.'login');
    }

    public function login(Request $req){
        $this->validation($req);
        $input = $req->only('email', 'password');
        
        if($this->guard()->attempt($input, $req->filled('remember'))){
            return redirect()->route($this->route.'dashboard');
        }

        swal()->error('Gagal', 'Login gagal');
        return redirect()->back();
    }

    protected function validation(Request $req){
        $req->validate([
            'email'    => 'required|email|max: 50',
            'password' => 'required|string'
        ]);
    }

    protected function guard(){
        return Auth::guard();
    }

    public function logout(){
        $this->guard()->logout();
        return redirect()->route($this->route.'dashboard');
    }
}