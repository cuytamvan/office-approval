<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\AgentRequest;

use App\Models\Agent;

use JsValidator;
use Datatables;
use View;
use Hash;
use Auth;

class AgentController extends Controller{
    protected $model;
    protected $title = 'Agent';
    protected $view = 'admin.agent.';
    protected $route = 'admin.agent.';
    protected $validation = 'App\Http\Requests\Admin\AgentRequest';

    public function __construct(Agent $model){
        $this->model = $model;

        View::share('route', $this->route);
        View::share('title', $this->title);
    }

    protected function user(){
        return Auth::guard()->user();
    }

    public function index(){
        View::share('breadcrumb', [
            [$this->title, route($this->route.'index')],
        ]);

        return view($this->view.'index');
    }

    public function data_api(){
        return Datatables::of($this->model->whereStatus(1))->make(true);
    }
    
    
    public function confirmation(){
        View::share('breadcrumb', [
            [$this->title, route($this->route.'index')],
        ]);
        
        return view($this->view.'confirmation');
    }

    public function data_api_confirmation(){
        return Datatables::of($this->model->whereStatus(0))->make(true);
    }

    public function confirmStatus(Request $req){
        $data = $this->model->find($req->id);
        if($data){
            $status = $data->status == '1' ? 0 : 1;
            if($data->update(['status' => $status])){
                $res['success'] = true;
                $res['data']    = $data;
            }else{
                $res['success'] = false;
                $res['data']    = [];
            }
        }else{
            $res['success'] = false;
            $res['data']    = [];
        }
        return $res;
    }
    
    public function create(){
        View::share('breadcrumb', [
            [$this->title, route($this->route.'index')],
            ['Tambah '.$this->title, route($this->route.'create')]
        ]);
        $validator = JsValidator::formRequest($this->validation);

        return view($this->view.'create', compact('validator'));
    }

    public function store(AgentRequest $req){
        $input = $req->all();
        $input['password'] = Hash::make($input['password']);
        $data  = $this->model->create($input);
        if($data){
            swal()->success('Berhasil', 'Data telah berhasil disimpan');
            return redirect()->route($this->route.'confirm');
        }
        swal()->error('Gagal', 'Data telah gagal disimpan');
        return redirect()->back();
    }

    public function show($id){
        View::share('breadcrumb', [
            [$this->title, route($this->route.'index')],
            ['Tambah '.$this->title, route($this->route.'show', $id)]
        ]);
        $data = $this->model->findOrFail($id);

        return view($this->view.'show', compact('data'));
    }

    public function edit($id){
        View::share('breadcrumb', [
            [$this->title, route($this->route.'index')],
            ['Tambah '.$this->title, route($this->route.'edit', $id)]
        ]);
        $validator = JsValidator::formRequest($this->validation);
        $data = $this->model->findOrFail($id);

        return view($this->view.'edit', compact('data','validator', 'id'));
    }

    public function update(AgentRequest $req, $id){
        $input = $req->all();
        if(isset($input['password'])){
            $input['password'] = Hash::make($input['password']);
        }else{
            unset($input['password']);
        }
        $data  = $this->model->findOrFail($id);
        if($data->update($input)){
            swal()->success('Berhasil', 'Data telah berhasil disimpan');
            return redirect()->route($this->route.'index');
        }
        swal()->error('Gagal', 'Data telah gagal disimpan');
        return redirect()->back();
    }

    public function destroy($id){
        $data = $this->model->findOrFail($id);
        if($data->delete()){
            swal()->success('Berhasil', 'Data telah berhasil dihapus');
            return redirect()->route($this->route.'index');
        }
        swal()->error('Gagal', 'Data telah gagal dihapus');
        return redirect()->back();
    }
}
