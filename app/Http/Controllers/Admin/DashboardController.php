<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Teacher;
use App\Models\Student;
use App\Models\Admin;

use View;

class DashboardController extends Controller{
    protected $title = 'Dashboard';
    protected $view  = 'admin.';
    protected $route = 'admin.';

    public function __construct(){
        View::share('route', $this->route);
        View::share('title', $this->title);
    }

    public function index(){
        return view($this->view.'dashboard');
    }
}
