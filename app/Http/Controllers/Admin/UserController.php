<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\Admin\UserRequest;

use App\Models\User;

use JsValidator;
use Datatables;
use View;
use Hash;
use Auth;

class UserController extends Controller
{
    protected $model;
    protected $title = 'Admin';
    protected $view = 'admin.user.';
    protected $route = 'admin.user.';
    protected $validation = 'App\Http\Requests\Admin\UserRequest';

    public function __construct(User $model){
        $this->model = $model;

        View::share('route', $this->route);
        View::share('title', $this->title);
    }

    protected function user(){
        return Auth::guard()->user();
    }

    public function index(){
        View::share('breadcrumb', [
            [$this->title, route($this->route.'index')],
        ]);

        return view($this->view.'index');
    }

    public function data_api(){
        return Datatables::of($this->model->query())->make(true);
    }
    
    public function create(){
        View::share('breadcrumb', [
            [$this->title, route($this->route.'index')],
            ['Tambah '.$this->title, route($this->route.'create')]
        ]);
        $validator = JsValidator::formRequest($this->validation);

        return view($this->view.'create', compact('validator'));
    }

    public function store(UserRequest $req){
        $input = $req->all();
        $input['password'] = Hash::make($input['password']);
        $data  = $this->model->create($input);
        if($data){
            swal()->success('Berhasil', 'Data telah berhasil disimpan');
            return redirect()->route($this->route.'index');
        }
        swal()->error('Gagal', 'Data telah gagal disimpan');
        return redirect()->back();
    }

    public function show($id){
        View::share('breadcrumb', [
            [$this->title, route($this->route.'index')],
            ['Tambah '.$this->title, route($this->route.'show', $id)]
        ]);
        $data = $this->model->findOrFail($id);

        return view($this->view.'show', compact('data'));
    }

    public function edit($id){
        View::share('breadcrumb', [
            [$this->title, route($this->route.'index')],
            ['Tambah '.$this->title, route($this->route.'edit', $id)]
        ]);
        $validator = JsValidator::formRequest($this->validation);
        $data = $this->model->findOrFail($id);

        return view($this->view.'edit', compact('data','validator', 'id'));
    }

    public function update(UserRequest $req, $id){
        $input = $req->all();
        if(isset($input['password'])){
            $input['password'] = Hash::make($input['password']);
        }else{
            unset($input['password']);
        }
        $data  = $this->model->findOrFail($id);
        if($data->update($input)){
            swal()->success('Berhasil', 'Data telah berhasil disimpan');
            return redirect()->route($this->route.'index');
        }
        swal()->error('Gagal', 'Data telah gagal disimpan');
        return redirect()->back();
    }

    public function destroy($id){
        if($id == $this->user()->id){
            swal()->error('Gagal', 'User sedang anda gunakan');
            return redirect()->back();
        }
        $data = $this->model->findOrFail($id);
        if($data->delete()){
            swal()->success('Berhasil', 'Data telah berhasil dihapus');
            return redirect()->route($this->route.'index');
        }
        swal()->error('Gagal', 'Data telah gagal dihapus');
        return redirect()->back();
    }
}
