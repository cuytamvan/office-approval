<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use JsValidator;
use Datatables;
use View;
use Hash;
use Auth;

class TransactionController extends Controller{
    protected $model;
    protected $title = 'Transaksi';
    protected $view = 'admin.transaction.';
    protected $route = 'admin.transaction.';

    public function __construct(){
        View::share('route', $this->route);
        View::share('title', $this->title);
    }

    protected function user(){
        return Auth::guard()->user();
    }

    public function index(){
        View::share('breadcrumb', [
            [$this->title, route($this->route.'index')],
        ]);

        return view($this->view.'index');
    }

    public function show($id){
        View::share('breadcrumb', [
            [$this->title, route($this->route.'index')],
            ['Tambah '.$this->title, route($this->route.'show', $id)]
        ]);
        $data = $this->model->findOrFail($id);

        return view($this->view.'show', compact('data'));
    }
}
