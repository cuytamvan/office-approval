<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;

use Hash;
use Auth;
use View;

class ProfileController extends Controller{
    protected $model;
    protected $title = 'Profile';

    public function __construct(User $model){
        $this->model = $model;
        View::share('title', $this->title);
    }

    public function index(){
        $user = $this->user();
        return view('admin.profile', compact('user'));
    }

    protected function user(){
        return Auth::guard()->user();
    }

    public function store(Request $req){
        $id    = $this->user()->id;
        $input = $req->only('name', 'password');
        $data  = $this->model->find($id);

        if(!is_null($input['password'])){
            $req->validate([
                'password' => 'confirmed'
            ]);

            $input['password'] = Hash::make($input['password']);
        }else{
            unset($input['password']);
        }

        if($data->update($input)){
            swal()->success('Berhasil', 'Data telah berhasil disimpan');
            return redirect()->route('admin.profile');
        }
        swal()->error('Gagal', 'Data telah gagal disimpan');
        return redirect()->back();
    }
}