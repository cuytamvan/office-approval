<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use View;
use Auth;

class TestController extends Controller{    
    protected $model;
    protected $validation;

    protected $title = 'Dashboard';
    protected $route = 'dashboard';
    protected $view  = '';

    public function __construct(){
        View::share('title', $this->title);
        View::share('route', $this->route);
    }

    protected function breadcrumb(array $param){
        View::share('breadcrumb', $param);
    }

    public function index(){
        $this->breadcrumb([
            ['text' => 'Icon', 'link' => route($this->route.'.index')],
            ['text' => 'Flaticon', 'link' => route($this->route.'.index')],
        ]);
        return view('test');
    }
}
