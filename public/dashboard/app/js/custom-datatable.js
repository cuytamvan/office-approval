var options = {
  data: {
    type: 'remote',
    source: {
      read: {
        url: url_data,
        method: 'GET',
        headers: { 'x-my-custom-header': 'some value', 'x-test-header': 'the value'},
        params: {
          generalSearch: '',
          EmployeeID   : 1,
          someParam    : 'someValue',
          token        : '{{ csrf_token() }}'
        },
        map: function(raw) {
            var dataSet = raw;
            if (typeof raw.data !== 'undefined') {
                  dataSet = raw.data;
            }
            return dataSet;
        },
      }
    },
    pageSize : 10,
    saveState: {
      cookie    : true,
      webstorage: true
    },

    serverPaging   : false,
    serverFiltering: false,
    serverSorting  : false
  },

  layout: {
    theme: 'default',
    class: 'm-datatable--brand',
    scroll: false,
    height: null,
    footer: false,
    header: true,

    smoothScroll: {
      scrollbarShown: true
    },

    spinner: {
      overlayColor: '#000000',
      opacity     : 0,
      type        : 'loader',
      state       : 'brand',
      message     : true
    },

    icons: {
      sort      : {asc: 'la la-arrow-up', desc: 'la la-arrow-down'},
      pagination: {
        next : 'la la-angle-right',
        prev : 'la la-angle-left',
        first: 'la la-angle-double-left',
        last : 'la la-angle-double-right',
        more : 'la la-ellipsis-h'
      },
      rowDetail: {expand: 'fa fa-caret-down', collapse: 'fa fa-caret-right'}
    }
  },
  sortable  : false,
  pagination: true,
  search    : {
    onEnter: false,
    input  : $('#generalSearch'),
    delay  : 400,
  },

  rows: {
    callback: function() {},
    autoHide: false,
  },
  columns: custom_column,

  toolbar: {
    layout: ['pagination', 'info'],

    placement: ['bottom'],

    items: {
      pagination: {
        type: 'default',

        pages: {
          desktop: {
            layout: 'default',
            pagesNumber: 6
          },
          tablet: {
            layout: 'default',
            pagesNumber: 3
          },
          mobile: {
            layout: 'compact'
          }
        },

        navigation: {
          prev: true,
          next: true,
          first: true,
          last: true
        },
        pageSizeSelect: [10, 20, 30, 50, 100]
      },
      info: true
    }
  },

  translate: {
    records: {
      processing: 'Tunggu...',
      noRecords: 'Tidak ada data'
    },
    toolbar: {
      pagination: {
        items: {
          default: {
            first: 'First',
            prev: 'Previous',
            next: 'Next',
            last: 'Last',
            more: 'More pages',
            input: 'Page number',
            select: 'Select page size'
          },
          info: 'Menampilkan {{start}} - {{end}} dari {{total}} data'
        }
      }
    }
  }
}
var datatable = $('.my_datatable').mDatatable(options);
$('body').on('click', '.delete-from-table', function(e){
  e.preventDefault();
  swal({
    title             : 'Hapus data tersebut?',
    text              : "Data tidak bisa dikembalikan jika sudah terhapus",
    type              : 'warning',
    showCancelButton  : true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor : '#d33',
    confirmButtonText : 'Yes, Hapus!'
  }).then((result) => {
    if (result.value) {
      $(this).parent().submit();
    }
  })
});