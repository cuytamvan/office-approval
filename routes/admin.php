<?php
/**
 * 
 * Custom route for admin
 * 
 */
Route::group(['middleware' => ['guest']], function(){
  Route::get('login', 'LoginController@showLoginForm')->name('login');
  Route::post('login', 'LoginController@login')->name('login_action');
});

Route::group(['middleware' => ['auth']], function(){
  Route::post('logout', 'LoginController@logout')->name('logout');
  Route::get('', 'DashboardController@index')->name('dashboard');

  // profile
  Route::get('profile', 'ProfileController@index')->name('profile');
  Route::post('profile', 'ProfileController@store')->name('profile_action');

  // user 
  Route::get('user/api', 'UserController@data_api')->name('user.data_api');
  Route::resource('user', 'UserController');

  // agent 
  Route::group(['prefix' => 'agent', 'as' => 'agent.'], function(){
    Route::get('api', 'AgentController@data_api')->name('data_api');
    Route::get('api-confirmation', 'AgentController@data_api_confirmation')->name('data_api_confirmation');
    Route::get('confirmation', 'AgentController@confirmation')->name('confirmation');
    Route::post('confirmation', 'AgentController@confirmStatus')->name('confirmation.update');
  });
  Route::resource('agent', 'AgentController');

  Route::get('transaction', 'TransactionController@index')->name('transaction.index');
});